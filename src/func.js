const getSum = (str1, str2) => {
    if(typeof(str1)!=="string"||typeof(str2)!=="string"){
      return false;
    }
    const val=/^\d*$/
   if(!val.test(str1)||!val.test(str2))
   return false;
   res=BigInt(str1)+BigInt(str2);
    return res.toString();
};

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  var comments=0;
  let posts=0;
  for(let i of listOfPosts){
    if(i.author===authorName){
      posts++;
    }
  }
  for(let j in listOfPosts){
    for (let y in listOfPosts[j].comments){
      if(listOfPosts[j].comments[y].author===authorName){
        comments++;
      }
    }
  }
  return `Post:${posts},comments:${comments}`;


};

const tickets=(people)=> {
  let money = 0;
 for(i of people){
  if(i===25){
    money+=i;
  }else{
    money= money-(i-25)
  }
  if(money<0){
    return 'NO'
  }
  money+=25;
 }
 return "YES"
};


module.exports = {getSum, getQuantityPostsByAuthor, tickets};
